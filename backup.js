if (localStorage.getItem("translations") == null) {
  localStorage.setItem("translations", JSON.stringify(new Array()));
}
let translations = JSON.parse(localStorage.getItem("translations"));
let selectedFile = null;
let displayedTranslation = null;

const fileSelect = document.querySelector("#fileSelect");
const listContainer = document.querySelector("#listContainer");

fileSelect.addEventListener("click", function(e) {
  preventDefaults(e);
  listContainer.classList.toggle("visible");
  listContainer.classList.toggle("hidden");
});

document.body.addEventListener("click", function(e) {
  listContainer.classList.remove("visible");
  listContainer.classList.add("hidden");
})

async function saveTranslations() {
  localStorage.setItem("translations", JSON.stringify(translations));
}



function displayRecords() {
  listContainer.innerHTML = "";
  if (translations.length >0) {
  translations.forEach( rec => {
    let divEl = document.createElement("div");
    listContainer.appendChild(divEl);
    let fileButton = document.createElement("button");
    fileButton.innerHTML = rec.file;
    fileButton.classList.add("file");
    fileButton.value = rec.file;
    fileButton.addEventListener("click", handleSelect);
    divEl.appendChild(fileButton);
    let eraseButton = document.createElement("button");
    eraseButton.innerHTML = '<i class="fa-solid fa-trash-can"></i>';
    eraseButton.classList.add("remove");
    eraseButton.value = rec.file;
    eraseButton.addEventListener("click", handleRemove);
    divEl.appendChild(eraseButton);
  })
 } else {
  listContainer.innerHTML = "No conversions to view";
 }
 displaySelectedTranslation();
}


function displaySelectedTranslation() {
  if (displayedTranslation != null) {
    fileSelect.innerHTML = displayedTranslation.file;
    output.value = displayedTranslation.text;
  } else {
    fileSelect.innerHTML = "Choose a file"
    output.value = "";
  }
}

const dropArea = document.querySelector('#dropArea');


function handleSelect(e) {
  translations.forEach(trans => {
    if (trans.file == e.target.value) {
      displayedTranslation = trans;
    }
  });
  displaySelectedTranslation();
}

function handleRemove(e) {
  translations = translations.filter(trans => {trans.file != e.target.value});
  saveTranslations();
  displayedTranslation = null;
  displayRecords();
}


['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
  dropArea.addEventListener(eventName, preventDefaults, false);
  document.body.addEventListener(eventName, preventDefaults, false);
});

function preventDefaults (e) {
  e.preventDefault();
  e.stopPropagation();
}

dropArea.addEventListener('drop', handleDrop, false);


const allowedFileTypes = ["image/png", "image/jpeg", "image/jpg"];

function handleDrop(e) {
  preventDefaults(e);
  let valid = Array.from(e.dataTransfer.files).filter(file => allowedFileTypes.includes(file.type));
  if (valid.length === 0) {
    alert("You did not insert png/jpg files!");
  } else {
    selectedFile = valid[0];
    displaySelectedFile();
  }
}

function displaySelectedFile() {
    document.querySelector("#list").innerHTML="";
    if (selectedFile !== null) {
        document.querySelector("#list").innerHTML = "<br>"+selectedFile.name;
    }
}

dropArea.addEventListener("change", function(e){
    selectedFile = e.target.files[0];
    displaySelectedFile();
})


class Translation {
  file;
  text;
  constructor(name) {
    this.file = name;
    this.text = "";
  };
  add(word) {
    this.text += " ";
    this.text += word;
  }
}

const output = document.querySelector("#output");
const form = document.querySelector("#input");

form.addEventListener("submit",  async function(e) {
  preventDefaults(e);
  if (selectedFile != null) {
    if (!navigator.onLine) {
      alert("You must be online!");
      return;
    }
    console.log("Transfer");
    let data = new FormData();
    data.append("image", selectedFile);
    let request = {
      method: "post",
      headers: {
        "x-api-key": "cEbnMrTf2kcvAPvHsEzYWg==Lzvc9LnsVbCzRQ8F"
      },
      body: data
    };
    let trans = new Translation(selectedFile.name);
    let httpResponse = await fetch("https://api.api-ninjas.com/v1/imagetotext", request)
    .catch((error) => {alert(error)});
    if (httpResponse.ok) {
      let data = await httpResponse.json();
      data.forEach(info => {trans.add(info.text)});
      output.append(trans.text);
      translations.push(trans);
      saveTranslations();
      displayedTranslation = trans;
      selectedFile = null;
      displayRecords();
      displaySelectedFile();
      } else {
        alert("Error " + httpResponse.status + " occured, message provided by server: " + httpResponse.json())
      }
  } else {
    alert("No files selected!");
  }
})

displayRecords();

async function greet() {
  let lat;
  let lon;
  navigator.geolocation.getCurrentPosition(async (position) => {
    lat = position.coords.latitude;
    lon = position.coords.longitude;
    if (lat==undefined || lon== undefined) {
      console.log("upsie");
      return;
    }
    let request = {
      method: "get",
      headers: {
        "x-api-key": "cEbnMrTf2kcvAPvHsEzYWg==Lzvc9LnsVbCzRQ8F"
      },
    };
    let url = 'https://api.api-ninjas.com/v1/reversegeocoding?lat=' + lat + '&lon=' + lon;
    let response = await fetch(url, request);
    console.log(response);
  })
}
//greet();