/**
 * class to represent img -> text conversion
*/
class Translation {
  file;
  text;
  constructor(name) {
    this.file = name;
    this.text = "";
  };
  add(word) {
    this.text += " ";
    this.text += word;
  }
}

const fileSelect = document.querySelector("#fileSelect");
const listContainer = document.querySelector("#listContainer");

/**
 * interface storing user's conversions into browser's localstorage
 */

class TranslationsManager {
  static translations = JSON.parse(localStorage.getItem("translations")) ?? [];
  static translation = null;
  
  /**
   * takes record and adds it to list of records for displaying...also stores it to localstprage 
   */

  static async save(record) {
    if (record instanceof Translation) {
      TranslationsManager.translations.push(record);
      localStorage.setItem("translations", JSON.stringify(TranslationsManager.translations));
      TranslationsManager.displayTranslations();
    }
  }

  /**
   * handles user's action of deleting a conversion 
   */
  
  static async erase(e) {
    let val;
    if (e.target.tagName =="I") {
      val = e.target.parentNode.value;
    } else {
      val = e.target.value;
    }
    TranslationsManager.translations = TranslationsManager.translations.filter(trans => {
      return trans.file != val;
    });
    localStorage.setItem("translations", JSON.stringify(TranslationsManager.translations));
    TranslationsManager.displayTranslations();
  }
  
  /**
   *displays all translations in a window for user to select or erase  
   */

  static displayTranslations() {
    listContainer.innerHTML = "";
    if (TranslationsManager.translations.length >0) {
      TranslationsManager.translations.forEach( rec => {
        let divEl = document.createElement("div");
        listContainer.appendChild(divEl);
        let fileButton = document.createElement("button");
        fileButton.innerHTML = rec.file;
        fileButton.classList.add("file");
        fileButton.value = rec.file;
        fileButton.addEventListener("click", TranslationsManager.displaySelectedTranslation);
        divEl.appendChild(fileButton);
        let eraseButton = document.createElement("button");
        eraseButton.innerHTML = '<i class="fa-solid fa-trash-can"></i>';
        eraseButton.classList.add("remove");
        eraseButton.value = rec.file;
        eraseButton.addEventListener("click", TranslationsManager.erase);
        divEl.appendChild(eraseButton);
      })
    } else {
      listContainer.innerHTML = "No conversions to view";
    }
  }

  /**
   * displays name of currently viewed file 
   */

  static displaySelectedTranslation(e) {
    if (e != null) {
      TranslationsManager.translations.forEach(trans => {
        if (trans.file == e.target.value) {
          TranslationsManager.translation = trans;
        }
      });
    }
    if (TranslationsManager.translation != null) {
      fileSelect.innerHTML = TranslationsManager.translation.file;
      output.value = TranslationsManager.translation.text;
    } else {
      fileSelect.innerHTML = "Choose a file";
      output.value = "";
    }
  }
}

TranslationsManager.displayTranslations();


/**
 * gets user's location(longtitude and latitude) and via Ninjas gets location name, which is then displayed
 */
async function greet() {
  navigator.geolocation.getCurrentPosition(async (position) => {
    let lat = position.coords.latitude;
    let lon = position.coords.longitude;
    let request = {
      method: "get",
      headers: {
        "x-api-key": "cEbnMrTf2kcvAPvHsEzYWg==Lzvc9LnsVbCzRQ8F"
      },
    };
    let url = 'https://api.api-ninjas.com/v1/reversegeocoding?lat=' + lat + '&lon=' + lon;
    let response = await fetch(url, request);
    if (response.ok) {
      let message = await response.json();
      console.log(message[0]);
      document.querySelector("#location").innerHTML = message[0].name + ",<br> " + message[0].country;
    }
  });
}

greet();

//displays or hides translations if fileSelect button is pressed
fileSelect.addEventListener("click", function(e) {
  preventDefaults(e);
  listContainer.classList.toggle("visible");
  listContainer.classList.toggle("hidden");
});

//hides translations with click on the screen(selected translation or hidden translations)
document.body.addEventListener("click", function(e) {
  listContainer.classList.remove("visible");
  listContainer.classList.add("hidden");
})

const dropArea = document.querySelector('#dropArea');

//if user drag&drops file into our page, we prevent browser from opening this file
['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
  dropArea.addEventListener(eventName, preventDefaults, false);
  document.body.addEventListener(eventName, preventDefaults, false);
});

function preventDefaults (e) {
  e.preventDefault();
  e.stopPropagation();
}

const list = document.querySelector("#list");
const allowedFileTypes = ["image/png", "image/jpeg", "image/jpg"];
const loadContainer = document.querySelector("#loadContainer");


/*
** interface handling and storing files to convert 
*/
class InputHandler {
  static selectedFile = null;
  static failSound = new Audio("fail.mp3");
  static successSound = new Audio("success.mp3");

  /*
  **gets dropped file and if its valid, saves it as file for conversion
  */

  static handleDrop(e) {
    preventDefaults(e);
    let valid = Array.from(e.dataTransfer.files).filter(file => allowedFileTypes.includes(file.type));
    if (valid.length === 0) {
      alert("You did not insert png/jpg files!");
    } else {
      InputHandler.selectedFile = valid[0];
      InputHandler.displaySelectedFile();
    }
  }

  /*
  **if file is inserted via form field, it is stored as file for conversion
  */
 
  static handleChange(e) {
    InputHandler.selectedFile = e.target.files[0];
    InputHandler.displaySelectedFile();
  }

  /**
   * displays name of currently selected file
   */
  static displaySelectedFile() {
    list.innerHTML="";
    if (InputHandler.selectedFile !== null) {
      list.innerHTML = "<br>"+InputHandler.selectedFile.name;
    }
  }
  /**
   * transfers a file for conversion, and stores it to localstorage 
   * conversion is then displayed
   * user is told if some error occurs
   */
  static async handleTransmition(e) {
    preventDefaults(e);
    if (InputHandler.selectedFile != null) {
      if (!navigator.onLine) {
        InputHandler.failSound.play();
        alert("You must be online!");
        return;
      }
      InputHandler.successSound.play();
      loadContainer.classList.remove("hidden");
      loadContainer.classList.add("loading");
      let data = new FormData();
      data.append("image", InputHandler.selectedFile);
      let request = {
        method: "post",
        headers: {
          "x-api-key": "cEbnMrTf2kcvAPvHsEzYWg==Lzvc9LnsVbCzRQ8F"
        },
        body: data
      };
      let trans = new Translation(InputHandler.selectedFile.name);
      let httpResponse = await fetch("https://api.api-ninjas.com/v1/imagetotext", request)
      .catch((error) => {alert(error)});
      if (httpResponse.ok) {
        let data = await httpResponse.json();
        data.forEach(info => {trans.add(info.text)});
        output.append(trans.text);
        TranslationsManager.save(trans);
        TranslationsManager.translation = trans;
        TranslationsManager.displaySelectedTranslation();
        InputHandler.selectedFile = null;
        InputHandler.displaySelectedFile();
        loadContainer.classList.add("hidden");
        loadContainer.classList.remove("loading");
        } else {
          alert("Error " + httpResponse.status + " occured, message provided by server: " + httpResponse.json())
        }
    } else {
      InputHandler.failSound.play();
      alert("No files selected!");
    }
  }
}


/**
 * to react on files dropping in window
*/
dropArea.addEventListener('drop', InputHandler.handleDrop, false);

/**
 * to react on users input of files via form button
 */
dropArea.addEventListener("change", InputHandler.handleChange, false);
  
const output = document.querySelector("#output");
const form = document.querySelector("#input");

/**
 * if user sends image to convert
 */
form.addEventListener("submit",  InputHandler.handleTransmition);

const copyButton = document.querySelector("#copy");

/**
 *when user copies text of transition 
 */
copyButton.addEventListener("click", function() {
  output.select();
  navigator.clipboard.writeText(output.value);
  copyButton.classList.add("checked");
  setTimeout(function() {
    copyButton.classList.remove("checked");
  }, 500);
})