const canv = document.querySelector("#background");

/**
 * interface responsible for canvas background animation
 */

class BackgroundAnimator {
  static context = canv.getContext("2d");
  static previous = performance.now() / 1000;
  static time;
  static points;
  static width;
  static height;

  /**
   * animation cycle, in which delta time is computed and each point is then redrawed
   */

  static doAnimation() {
    canv.height = window.innerHeight;
    canv.width = window.innerWidth;
    BackgroundAnimator.height = canv.height;
    BackgroundAnimator.width = canv.width;
    BackgroundAnimator.context.clearRect(0,0, BackgroundAnimator.width, BackgroundAnimator.height);
    BackgroundAnimator.time = performance.now()/1000 - BackgroundAnimator.previous;
    BackgroundAnimator.previous += BackgroundAnimator.time;
    BackgroundAnimator.points.forEach( point => {
      point.animate();
    });
    //console.log("Resolution: " + BackgroundAnimator.height + " x " + BackgroundAnimator.width);    //to get a resolution of canvas
    requestAnimationFrame(BackgroundAnimator.doAnimation);
  }

  /**
   * generates 25 points for background animation
   */
  static generatePoints() {
    BackgroundAnimator.points = [];
    for (let index = 0; index < 25; index++) {
      BackgroundAnimator.points[index] = new Point(index);
    }
  } 
}


/**
 * class for rendering circles in backround
 */
class Point {
  static colors = ["purple", "magenta", "green", "orange", "cyan", "pink"];
  #xTransformer;
  #yTransformer;
  #x;
  #y;
  #size;
  #color;
  /**
   * creates a circle of random size and color, with random starting position and random trajectory
   */
  constructor () {
    if (Math.random() >= 0.5) {
      this.#xTransformer = Math.random();
    } else {
      this.#xTransformer = Math.random() * -1;
    }
    if (Math.random() >= 0.5) {
      this.#yTransformer = Math.random();
    } else {
      this.#yTransformer = Math.random() * -1;
    }
    this.#x= Math.random();
    this.#y = Math.random();
    this.#color = Point.colors[Math.floor( Math.random() *6)];
    this.#size = Math.floor(Math.random() *20)+15;
  }

  /**
  * updates cirlces position with delta time and draws it in new position 
  */
  animate() {
    BackgroundAnimator.context.fillStyle = this.#color;
    BackgroundAnimator.context.beginPath();
    this.#x = (this.#x + (BackgroundAnimator.time * (1/this.#size) * this.#xTransformer)%1);
    if (this.#x <= 0 ) {
      this.#x +=1;
    }
    if (this.#x >= 1) {
      this.#x -=1;
    }
    this.#y = (this.#y + (BackgroundAnimator.time * (1/this.#size) * this.#yTransformer)%1);
    if (this.#y <= 0 ) {
      this.#y +=1;
    }
    if (this.#y >= 1) {
      this.#y -=1;
    }
    let x = this.#x * BackgroundAnimator.width;
    let y = this.#y * BackgroundAnimator.height;
    BackgroundAnimator.context.arc(x, y, this.#size, 0, Math.PI *2 );
    BackgroundAnimator.context.fill();
    BackgroundAnimator.context.closePath();
  }
}

BackgroundAnimator.generatePoints();
BackgroundAnimator.doAnimation();